/*
   Copyright (c) 2013, The Linux Foundation. All rights reserved.
   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are
   met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.
   THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
   ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
   BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
   BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
   WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
   OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
   IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include <errno.h>
#define _REALLY_INCLUDE_SYS__SYSTEM_PROPERTIES_H_
#include <sys/_system_properties.h>
#include <android-base/properties.h>

#include "init.h"
#include "vendor_init.h"
#include "property_service.h"
#include "log.h"
#include "util.h"
#include "service.h"
#include <stdio.h>
#include <string.h>

void property_override(char const prop[], char const value[])
{
    prop_info *pi;

    pi = (prop_info*) __system_property_find(prop);
    if (pi)
        __system_property_update(pi, value, strlen(value));
    else
        __system_property_add(prop, strlen(prop), value, strlen(value));
}

void gsm_properties()
{
    property_set("cm.icera.enabled", "1");
    property_set("ril.icera-config-args", "notifier:ON,datastall:ON,lwaactivate");
    property_set("gsm.modem.power.device", "/sys/devices/platform/tegra_usb_modem_power/modem_reset/value,0,1");
    property_set("gsm.modem.edp.device", "/sys/power/sysedp");
    property_set("gsm.modem.edp.state", "/sys/devices/platform/sysedp_modem/sysedp_state");
    property_set("ro.ril.devicename", "/dev/ttyACM0");
    property_set("mdc_initial_max_retry", "10");
    property_set("rild.libpath", "libril-icera.so");
    property_set("rild.libargs", "-e rmnet0 -e rmnet0:0 -e rmnet0:1 -n");
    property_set("gsm.modem.crashlogs.directory", "/data/rfs/data/debug");
    property_set("ril.maxretries", "15");
    property_set("gsm.modem.powercontrol", "enabled");
    property_set("ro.ril.wake_lock_timeout", "200000");
    property_set("ro.telephony.default_network", "9");
}

namespace android {
namespace init {
void vendor_load_properties()
{
    std::string platform = "";
    std::string model = "";
    FILE  *fp = NULL;
    char  *board_info = NULL;
    char  *override = NULL;
    size_t len = 0;
    enum {
      hawkeye_unknown,
      hawkeye_na_wf,
      hawkeye_un_do,
      hawkeye_un_mo
    };
    int detected_model = hawkeye_unknown;

    platform = android::base::GetProperty("ro.board.platform", "");
    if (strncmp(platform.c_str(), ANDROID_TARGET, PROP_VALUE_MAX))
        return;

    // Check for persistent model override
    fp = fopen("/data/property/persist.cm.shield.model", "r");
    if (fp) {
        while ((getline(&override, &len, fp)) != (ssize_t)-1) {
            if (strstr(override, "hawkeye_"))
                break;
        }
        fclose(fp);

        if (override) {
            if (!strncmp(override, "hawkeye_na_wf", PROP_VALUE_MAX))
                detected_model = hawkeye_na_wf;
            else if (!strncmp(override, "hawkeye_na_do", PROP_VALUE_MAX) ||
                     !strncmp(override, "hawkeye_un_do", PROP_VALUE_MAX))
                detected_model = hawkeye_un_do;
            else if (!strncmp(override, "hawkeye_un_mo", PROP_VALUE_MAX))
                detected_model = hawkeye_un_mo;

            free(override);
        }
    }

    // If not overridden.
    if (detected_model == hawkeye_unknown) {
        // Get model from bootloader
	// This doesn't actually appear to be true. Don't know how to differentiate atm.
	// Default to wifi only to be safe.
        model = android::base::GetProperty("ro.hardware", "");
        if (!model.compare("hawkeye_un_mo"))
            detected_model = hawkeye_un_mo;
	else if (!model.compare("hawkeye_na_do") ||
	         !model.compare("hawkeye_un_do"))
            detected_model = hawkeye_un_do;
        else // Default to wifi only
            detected_model = hawkeye_na_wf;

        free(board_info);
    }

    switch (detected_model) {
        case hawkeye_na_wf:
            /* Wi-Fi Only */
            property_override("ro.build.fingerprint", "NVIDIA/hawkeye_na_wf/hawkeye:7.0/NRD90M/1749719_832.2408:user/release-keys");
            property_override("ro.build.description", "hawkeye_na_wf-user 7.0 NRD90M 1749719_832.2408 release-keys");
            property_override("ro.product.name", "hawkeye_na_wf");
            property_set("ro.radio.noril", "true");
            break;

        case hawkeye_un_do:
            /* Data Only, no difference, so just using hawkeye_un_do. */
            gsm_properties();
            property_override("ro.build.fingerprint", "NVIDIA/hawkeye_un_do/hawkeye:7.0/NRD90M/1749719_832.2408:user/release-keys");
            property_override("ro.build.description", "hawkeye_un_do-user 7.0 NRD90M 1749719_832.2408 release-keys");
            property_override("ro.product.name", "hawkeye_un_do");
            property_set("ro.modem.do", "1");
            break;

        case hawkeye_un_mo:
            /* Rest of World Voice (Device may not even exist, but is partially in upstream source) */
            gsm_properties();
            property_override("ro.build.fingerprint", "NVIDIA/hawkeye_un_mo/hawkeye:7.0/NRD90M/1749719_832.2408:user/release-keys");
            property_override("ro.build.description", "hawkeye_un_mo-user 7.0 NRD90M 1749719_832.2408 release-keys");
            property_override("ro.product.name", "hawkeye_un_mo");
            property_set("ro.modem.vc", "1");
            break;
    }

    property_override("ro.build.product", "hawkeye");
    property_override("ro.product.device", "hawkeye");
    property_override("ro.product.model", "SHIELD Tablet Pro");
}
}  // namespace init
}  // namespace android

int vendor_handle_control_message(const std::string &msg, const std::string &arg)
{
    Service *sf_svc = NULL;
    Service *zg_svc = NULL;

    if (!msg.compare("restart") && !arg.compare("consolemode")) {
        sf_svc = ServiceManager::GetInstance().FindServiceByName("surfaceflinger");
        zg_svc = ServiceManager::GetInstance().FindServiceByName("zygote");

        if (sf_svc && zg_svc) {
            zg_svc->Stop();
            sf_svc->Stop();
            sf_svc->Start();
            zg_svc->Start();
        }

        return 0;
    }

    return -EINVAL;
}
