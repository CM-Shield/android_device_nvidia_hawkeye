# Inherit device configuration for hawkeye.
$(call inherit-product, device/nvidia/hawkeye/full_hawkeye.mk)

# Inherit some common lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

PRODUCT_NAME := lineage_hawkeye
PRODUCT_DEVICE := hawkeye
